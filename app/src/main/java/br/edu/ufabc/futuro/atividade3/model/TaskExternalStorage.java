package br.edu.ufabc.futuro.atividade3.model;


import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import br.edu.ufabc.futuro.atividade3.SettingsActivity;

public class TaskExternalStorage implements TaskStorageStrategy {
    private static final String LOGTAG = TaskExternalStorage.class.getName();
    private ArrayList<Task> tasks;

    public TaskExternalStorage(ArrayList<Task> tasks) {
        this.tasks = tasks;
    }

    /**
     * Save the tasks to external storage
     *
     * @return true if the tasks could be saved, false otherwise
     */
    @Override
    public boolean save() {
        boolean status = true;
        String doc;

        if (isExternalStorageWritable()) {
            OutputStreamWriter writer;
            File file = new File(SettingsActivity.EXTERNAL_FILE);
            File parent = file.getParentFile();

            doc = TasksJSONSerializer.serialize(tasks);
            if (parent.isDirectory() || parent.mkdirs()) {
                try {
                    writer = new OutputStreamWriter(new FileOutputStream(file));
                    writer.write(doc);
                    writer.close();
                } catch (FileNotFoundException e) {
                    Log.e(LOGTAG, "Failed to create external file", e);
                    status = false;
                } catch (IOException e) {
                    Log.e(LOGTAG, "Failed to write to external file", e);
                    status = false;
                }
            } else {
                Log.e(LOGTAG, "Failed to create external file path");
                status = false;
            }
        } else
            status = false;

        return status;
    }

    /**
     * Load the tasks from external storage
     *
     * @return true if the tasks could be loaded, false otherwise
     */
    @Override
    public boolean load() {
        boolean status = true;

        if (isExternalStorageReadable()) {
            InputStreamReader isReader;
            BufferedReader bfReader;
            String line;
            StringBuilder sb = new StringBuilder();

            try {
                isReader = new InputStreamReader(new FileInputStream(
                        new File(SettingsActivity.EXTERNAL_FILE)));
                bfReader = new BufferedReader(isReader);

                while ((line = bfReader.readLine()) != null)
                    sb.append(line);
                tasks.clear();
                tasks.addAll(TasksJSONSerializer.deserialize(sb.toString()));
            } catch (FileNotFoundException e) {
                Log.e(LOGTAG, "Failed to open external storage", e);
                status = false;
            } catch (IOException e) {
                Log.e(LOGTAG, "Failed to save to external storage", e);
                status = false;
            }
        } else {
            status = false;
            Log.e(LOGTAG, "Failed to read from external storage");
        }

        return status;
    }

    @Override
    public ArrayList<Task> getDataset() {
        return tasks;
    }

    @Override
    public void setDataset(ArrayList<Task> tasks) {
        this.tasks = tasks;
    }

    /**
     * Check if the external storage has write permission for this app. Should be called at
     * every attempt to write to external storage
     *
     * @return true if it is writable, false otherwise
     */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /**
     * Check if external storage has read permission for the app. Should be called at
     * every attempt to read the external storage
     *
     * @return true in case it is readable, false otherwise
     */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }
}
