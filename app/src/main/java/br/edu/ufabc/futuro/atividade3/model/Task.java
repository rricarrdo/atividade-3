package br.edu.ufabc.futuro.atividade3.model;


public class Task {
    private String title;
    private int priority;
    private boolean done;

    public Task() {
        title = null;
        done = false;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

}
