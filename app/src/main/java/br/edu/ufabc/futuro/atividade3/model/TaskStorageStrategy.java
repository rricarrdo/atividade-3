package br.edu.ufabc.futuro.atividade3.model;


import java.util.ArrayList;

public interface TaskStorageStrategy {

    /**
     * Save a dataset to the storage
     *
     * @return true if saving from storage was successfull, false otherwise
     */
    public boolean save();

    /**
     * Load a dataset from the storage
     *
     * @return true if loading from storage was successfull, false otherwise
     */
    public boolean load();

    public ArrayList<Task> getDataset();

    public void setDataset(ArrayList<Task> tasks);
}
