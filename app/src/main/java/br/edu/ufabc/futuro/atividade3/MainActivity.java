package br.edu.ufabc.futuro.atividade3;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.ListView;
import android.widget.Toast;

import br.edu.ufabc.futuro.atividade3.model.TaskDao;


public class MainActivity extends ActionBarActivity {
    private ListView listView;
    private TaskDao dao;
    BaseAdapter adapter;
    boolean ascPriority = true;
    boolean ascState = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        init();
        populateList();
    }

    @Override
    protected void onPause() {
        super.onPause();
        dao.save();
    }

    private void init() {
        listView = (ListView) findViewById(R.id.task_list);
        dao = TaskDao.newInstance(this);
    }

    private void populateList() {
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        adapter = new TaskAdapter(this);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CheckedTextView v = (CheckedTextView) view;

                if (v.isChecked())
                    dao.checkTaskAt(position);
                else
                    dao.uncheckTaskAt(position);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            case R.id.action_add:
                startActivity(new Intent(this, TaskInsert.class));
                return true;
            case R.id.action_clean_completed:
                int quantidadeRemovida = dao.cleanCompletedTasks();
                Toast.makeText(this, quantidadeRemovida + " Tarefa(s) Concluída(s) Removida(s)", Toast.LENGTH_LONG).show();
                listView.invalidateViews();
                return true;
            case R.id.action_share_tasks:
                String content = dao.formatTaskToEmail();

                // Utilzando esse formato para forcar o uso de uma aplicacao de e-mail.
                Intent intentImplicito = new Intent(android.content.Intent.ACTION_SENDTO);
                intentImplicito.setData(Uri.parse("mailto:"));
                intentImplicito.putExtra(Intent.EXTRA_TEXT, content);

                if (intentImplicito.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(intentImplicito, 0);
                }
                return true;

            case R.id.action_orderby_priority:
                dao.orderByPriority(ascPriority);
                ascPriority = !ascPriority;
                adapter.notifyDataSetChanged();
                return true;

            case R.id.action_orderby_state:
                dao.orderByState(ascState);
                ascState = !ascState;
                adapter.notifyDataSetChanged();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
