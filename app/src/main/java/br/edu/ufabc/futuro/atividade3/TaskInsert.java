package br.edu.ufabc.futuro.atividade3;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import br.edu.ufabc.futuro.atividade3.model.Task;
import br.edu.ufabc.futuro.atividade3.model.TaskDao;


public class TaskInsert extends ActionBarActivity {
    private EditText title;
    private RatingBar priority;
    private Button button;
    private TaskDao dao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_insert);
        init();
        bindHandlers();
    }

    private void init() {
        title = (EditText) findViewById(R.id.title);
        priority = (RatingBar) findViewById(R.id.ratingPriority);
        button = (Button) findViewById(R.id.button_save);

        dao = TaskDao.newInstance(this);
    }

    private void bindHandlers() {
        final Context self = this;

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Task task = new Task();
                task.setTitle(title.getText().toString());
                task.setPriority((int) priority.getRating());
                dao.add(task);
                if (dao.save()) {
                    Intent intent = new Intent(self, MainActivity.class);

                    Toast.makeText(self, getString(R.string.task_add_success),
                            Toast.LENGTH_SHORT).show();
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                } else
                    Toast.makeText(self, getString(R.string.task_add_failure),
                            Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_task_insert, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
