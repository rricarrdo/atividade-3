package br.edu.ufabc.futuro.atividade3;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;

import java.io.File;

import br.edu.ufabc.futuro.atividade3.model.TaskDao;

public class SettingsActivity extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    public static final String INTERNAL_STORAGE_MODE = "1";
    public static final String EXTERNAL_STORAGE_MODE = "2";
    public static final String INTERNAL_FILE = "todo.dat";
    public static final String EXTERNAL_FILE = Environment.getExternalStoragePublicDirectory(
            Environment.DIRECTORY_DOCUMENTS) + File.separator + "Todo" +
            File.separator + INTERNAL_FILE;
    private ListPreference listPreference;
    private TaskDao dao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        init();
        bindHandlers();
    }

    private void init() {
        listPreference = (ListPreference) findPreference(getString(R.string.storage_mode_key));
        listPreference.setSummary(listPreference.getEntry());
        dao = TaskDao.newInstance(this);
    }

    private void bindHandlers() {
        listPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            /**
             * Set the summary of the preference to the currently selected entry
             */
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                int index = listPreference.findIndexOfValue((String) newValue);
                CharSequence[] entries = listPreference.getEntries();

                listPreference.setSummary(entries[index]);

                return true;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        String storage_mode_key = getString(R.string.storage_mode_key);
        String clean_data = getString(R.string.clean_data_key);

        if (key.equals(clean_data)) {
            CheckBoxPreference storagePref = (CheckBoxPreference) findPreference(key);
            if (storagePref.isChecked()) {
                dao.cleanAllData();
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        } else {
            ListPreference storagePref = (ListPreference) findPreference(key);
            String value = storagePref.getValue();

            if (key.equals(storage_mode_key)) {

                if (value.equals(INTERNAL_STORAGE_MODE))
                    dao.setStorageStrategy(TaskDao.INTERNAL_STORAGE);
                else if (value.equals(EXTERNAL_STORAGE_MODE))
                    dao.setStorageStrategy(TaskDao.EXTERNAL_STORAGE);
                dao.save();
            }
        }
    }
}
