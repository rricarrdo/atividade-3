package br.edu.ufabc.futuro.atividade3;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.ListView;

import br.edu.ufabc.futuro.atividade3.model.Task;
import br.edu.ufabc.futuro.atividade3.model.TaskDao;

public class TaskAdapter extends BaseAdapter {
    LayoutInflater inflater;
    private TaskDao dao;
    private Context context;

    public TaskAdapter(Context c) {
        context = c;
        dao = TaskDao.newInstance(context);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        configureDao();
    }

    private void configureDao() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String storage_mode = prefs.getString(context.getString(R.string.storage_mode_key), "");

        if (storage_mode.equals(SettingsActivity.INTERNAL_STORAGE_MODE))
            dao.setStorageStrategy(TaskDao.INTERNAL_STORAGE);
        else if (storage_mode.equals(SettingsActivity.EXTERNAL_STORAGE_MODE))
            dao.setStorageStrategy(TaskDao.EXTERNAL_STORAGE);
        dao.load();
    }

    @Override
    public int getCount() {
        return dao.size();
    }

    @Override
    public Object getItem(int position) {
        return dao.getTaskAt(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CheckedTextView taskItem;
        Task task = dao.getTaskAt(position);
        ListView listParent = (ListView) parent;

        if (convertView == null) {
            convertView = inflater.inflate(android.R.layout.simple_list_item_checked, null);
        }
        taskItem = (CheckedTextView) convertView.findViewById(android.R.id.text1);
        taskItem.setText(task.getTitle() + " - Prioridade: " + task.getPriority());

        listParent.setItemChecked(position, task.isDone());

        return convertView;
    }
}
