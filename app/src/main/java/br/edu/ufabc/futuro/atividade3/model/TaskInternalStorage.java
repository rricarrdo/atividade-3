package br.edu.ufabc.futuro.atividade3.model;


import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import br.edu.ufabc.futuro.atividade3.SettingsActivity;

public class TaskInternalStorage implements TaskStorageStrategy {

    private static final String LOGTAG = TaskInternalStorage.class.getName();
    private ArrayList<Task> tasks;
    private Context context;

    public TaskInternalStorage(ArrayList<Task> tasks, Context context) {
        this.tasks = tasks;
        this.context = context;
    }

    /**
     * Save the tasks to internal storage
     *
     * @return true if the tasks could be saved, false otherwise
     */
    @Override
    public boolean save() {
        OutputStreamWriter writer;
        String doc = TasksJSONSerializer.serialize(tasks);
        boolean status = true;

        try {
            writer = new OutputStreamWriter(context.openFileOutput(SettingsActivity.INTERNAL_FILE,
                    Context.MODE_PRIVATE));
            writer.write(doc);
            writer.close();
        } catch (FileNotFoundException e) {
            Log.e(LOGTAG, "Failed to open internal storage", e);
            status = false;
        } catch (IOException e) {
            Log.e(LOGTAG, "Failed to save to internal storage", e);
            status = false;
        }

        return status;
    }

    /**
     * Load the tasks from internal storage
     *
     * @return true if the tasks could be loaded, false otherwise
     */
    @Override
    public boolean load() {
        boolean status = true;
        InputStreamReader isReader;
        BufferedReader bfReader;
        StringBuilder builder;
        String line;

        try {
            isReader = new InputStreamReader(context.openFileInput(SettingsActivity.INTERNAL_FILE));
            bfReader = new BufferedReader(isReader);
            builder = new StringBuilder();

            while ((line = bfReader.readLine()) != null)
                builder.append(line);
            tasks.clear();
            tasks.addAll(TasksJSONSerializer.deserialize(builder.toString()));
        } catch (FileNotFoundException e) {
            Log.e(LOGTAG, "Failed to open internal storage", e);
            status = false;
        } catch (IOException e) {
            Log.e(LOGTAG, "Failed to read from internal storage", e);
            status = false;
        }

        return status;
    }

    @Override
    public ArrayList<Task> getDataset() {
        return tasks;
    }

    @Override
    public void setDataset(ArrayList<Task> tasks) {
        this.tasks = tasks;
    }
}
