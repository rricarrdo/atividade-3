package br.edu.ufabc.futuro.atividade3.model;

import android.content.Context;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class TaskDao {
    public static final int INTERNAL_STORAGE = 1;
    public static final int EXTERNAL_STORAGE = 2;
    private static final String LOGTAG = TaskDao.class.getName();
    private static TaskDao dao;
    private ArrayList<Task> tasks;
    private Context context;
    private TaskStorageStrategy storage;

    protected TaskDao(Context c) {
        tasks = new ArrayList<Task>();
        context = c;
    }

    public static TaskDao newInstance(Context c) {
        if (dao == null) {
            dao = new TaskDao(c);
        } else
            dao.context = c;

        return dao;
    }

    public void setStorageStrategy(int strategy) {
        if (strategy == INTERNAL_STORAGE)
            storage = new TaskInternalStorage(tasks, context);
        else if (strategy == EXTERNAL_STORAGE)
            storage = new TaskExternalStorage(tasks);
    }

    public int size() {
        return tasks.size();
    }

    public void add(Task task) {
        tasks.add(task);
    }

    public void checkTaskAt(int position) {
        tasks.get(position).setDone(true);
    }

    public void uncheckTaskAt(int position) {
        tasks.get(position).setDone(false);
    }

    public Task getTaskAt(int position) {
        return tasks.get(position);
    }

    public boolean save() {
        return storage.save();
    }

    public boolean load() {
        return storage.load();
    }

    public int cleanCompletedTasks() {
        int tasksRemovidas = 0;

        // Verifica todas as tasks para encontrar as que estao concluidas.
        // Faz o for descrescente para, quando remover uma tarefa, nao perder o indice das proximas
        for (int iT = tasks.size() - 1; iT >= 0; iT--) {
            if (tasks.get(iT).isDone()) {
                tasks.remove(iT);
                tasksRemovidas++;
            }
        }
        // Persiste a lista de tarefas, com as tasks removidas.
        save();

        /* Obs. Quando o modo de armazenamento for modificado, a lista que esta em memoria sera
         * persistida para novo modo de armazenamento. Dessa forma, nao  eh necessario salvar em
         * todos os modos nesse momento
         */

        return tasksRemovidas;
    }

    public boolean cleanAllData() {

        try {
            tasks.clear();
            setStorageStrategy(INTERNAL_STORAGE);
            save();

            setStorageStrategy(EXTERNAL_STORAGE);
            save();

            return true;
        } catch (Exception e) {
            Toast.makeText(context, "Não foi possível limpar os dados. Exceção: " + e.getMessage(), Toast.LENGTH_LONG).show();
            return false;
        }

    }

    /**
     * Fomata todas as tarefas de uma forma legivel para se enviar por e-mail.
     *
     * @return String com as informacoes das tarefas para ser utilizada no conteudo do email.
     */
    public String formatTaskToEmail() {
        String content = "";

        for (Task task : tasks) {
            // Abre parenteses para mostrar o status da tarefa
            content += "( ";
            if (task.isDone()) {
                content += "X ";
            }
            // Fecha parenteses do status da tarefa
            // Abre colchetes da prioridade da tarefa
            content += ") [ ";

            for (int i = 0; i < task.getPriority(); i++) {
                content += "*";
            }

            // Fecha colchetes da prioridade da tarefa
            // Adiciona o Titulo da Tarefa e pula uma linha
            content += " ]" + task.getTitle() + "\n";
        }

        return content;
    }


    public void orderByPriority(final boolean ascPriority) {
        Collections.sort(tasks, new Comparator<Task>() {
            @Override
            public int compare(Task lhs, Task rhs) {
                if (ascPriority) {
                    return Integer.valueOf(lhs.getPriority()).compareTo(Integer.valueOf(rhs.getPriority()));
                } else {
                    return Integer.valueOf(rhs.getPriority()).compareTo(Integer.valueOf(lhs.getPriority()));
                }
            }
        });
    }

    public void orderByState(final boolean ascState) {
        Collections.sort(tasks, new Comparator<Task>() {
            @Override
            public int compare(Task lhs, Task rhs) {
                if (ascState) {
                    return Boolean.valueOf(lhs.isDone()).compareTo(Boolean.valueOf(rhs.isDone()));
                } else {
                    return Boolean.valueOf(rhs.isDone()).compareTo(Boolean.valueOf(lhs.isDone()));
                }
            }
        });
    }
}
