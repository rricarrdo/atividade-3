package br.edu.ufabc.futuro.atividade3.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class TasksJSONSerializer {

    private static final String LOGTAG = TasksJSONSerializer.class.getName();

    /**
     * Serialize an ArrayList of tasks into a JSON-encoded object
     *
     * @return true if the tasks could be serialized, false otherwise
     */
    public static String serialize(ArrayList<Task> tasks) {
        JSONObject rootNode = new JSONObject();

        try {
            JSONArray tasksArray = new JSONArray();

            for (Task task : tasks) {
                JSONObject taskObj = new JSONObject();

                taskObj.put("title", task.getTitle());
                taskObj.put("priority", task.getPriority());
                taskObj.put("done", task.isDone());
                tasksArray.put(taskObj);
            }
            rootNode.put("tasks", tasksArray);
        } catch (JSONException e) {
            Log.e(LOGTAG, "Failed to serialize task list", e);
        }

        return rootNode.toString();
    }

    /**
     * Deserialize JSON-encoded tasks to an ArrayList
     *
     * @return true if the tasks could be deserialized, false otherwise
     */
    public static ArrayList<Task> deserialize(String jsonStr) {
        JSONObject rootNode;
        ArrayList<Task> tasks = new ArrayList<>();

        try {
            rootNode = new JSONObject(jsonStr);
            JSONArray taskArray = rootNode.getJSONArray("tasks");

            tasks.clear();
            for (int i = 0; i < taskArray.length(); i++) {
                JSONObject taskObj = taskArray.getJSONObject(i);
                Task task = new Task();

                task.setTitle(taskObj.getString("title"));
                // Caso nao tenham sido adicionas prioridades, vem com prioridade 0 (zero).
                // getString do JSONObject lanca excessao caso a propriedade nao exista.
                try {
                    task.setPriority(Integer.valueOf(taskObj.getString("priority")));
                } catch (Exception e) {
                    task.setPriority(0);
                }

                task.setDone(taskObj.getBoolean("done"));
                tasks.add(task);
            }
        } catch (JSONException e) {
            Log.e(LOGTAG, "Failed to deserialize task list", e);
        }

        return tasks;
    }
}
